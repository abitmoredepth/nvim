"setlocal foldmethod=syntax
"autocmd!
set tabstop=4
set expandtab
set shiftwidth=4
set softtabstop=4

set encoding=utf-8
set fileencoding=utf-8

set colorcolumn=100

"" IndentLine Plugin Settings
let g:indentLine_color_term=22
